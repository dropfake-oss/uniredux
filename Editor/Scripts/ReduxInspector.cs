using UnityEditor;
using UnityEngine;
using UnityEngine.UIElements;
using UniRedux;
using System;
using UniRx;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Reflection;

namespace UniRedux.Tools
{
	class ReduxStoreData//<TState> where TState : class, new ()
	{
		//public ReduxStore<TState> Store;
		public Toggle StoreToggle;
		public Foldout StoreFoldout;
		public object LastState;
	}

    public class ReduxInspector : EditorWindow
    {
        [SerializeField]
        private VisualTreeAsset _visualTreeAsset = default;

        private static ReduxInspector? m_Instance;

		private ScrollView? actionScrollView;
		private Foldout? OptionsFoldout;
		private Dictionary<string, ReduxStoreData> StoreMap;
		private ScrollView? StateGroup;
		private Toggle? AutoScroll;
		private Toggle? CollapseToggle;
		private object? LastAction;
		private Foldout? LastFoldout;
		private int LastActionCount;
		private int TotalActionCount;
		private bool NeedsToScroll;
		private string? NullableString;
		private Label? DepthValueLabel;
		private int MaxDepth = 5; // optimize the object traversal by specifying how deep you'd like to go
		private Label? FilterTitle;
		private Color HighlightColor = new Color(80f / 255f, 80f / 255f, 80f / 255f);
		private List<IDisposable> Subscriptions = new List<IDisposable>();
		private const string PLAYER_PREFS_PREFIX = "ReduxInspector";
		private const string FILTER_PREFIX = "FilterFor";
		private const string SCROLL_SETTING = "ScrollSetting";
		private const string COLLAPSE_SETTING = "CollapseSetting";
		private const int LABEL_WIDTH = 170;

		private delegate void OnPurge();
		private OnPurge Purgers;

		private object LastState;

		public static ReduxInspector? Instance
        {
            get
            {
                if(m_Instance == null)
                {
                    return FindFirstInstance();
                }

                return m_Instance;
			}
        }

		public static ReduxInspector? FindFirstInstance()
		{
			var windows = (ReduxInspector[])Resources.FindObjectsOfTypeAll(typeof(ReduxInspector));
			if (windows.Length == 0)
				return null;
			return windows[0];
		}

		[MenuItem("DropFake/UniRedux Tools/UniRedux Inspector")]
        public static void ShowInspector()
        {
		    ReduxInspector wnd = GetWindow<ReduxInspector>();
			m_Instance = wnd;
		    wnd.titleContent = new GUIContent("UniRedux Inspector");
		}

		public void CreateGUI()
        {
			AddOptionsView();
			var splitView = new TwoPaneSplitView(0, 400, TwoPaneSplitViewOrientation.Vertical);
			splitView.style.flexGrow = 1;
			rootVisualElement.Add(splitView);

			MakeStateView(splitView);
			actionScrollView = new ScrollView(ScrollViewMode.VerticalAndHorizontal);
			actionScrollView.style.flexDirection = FlexDirection.ColumnReverse;
			actionScrollView.style.flexGrow = 1;
			splitView.Add(actionScrollView);

			var buttonContainer = new VisualElement();
			buttonContainer.style.flexDirection = FlexDirection.Row;
			rootVisualElement.Add(buttonContainer);

			var clearBtn = new Button();
			clearBtn.text = "Clear";
			clearBtn.style.flexGrow = 1;
			clearBtn.clickable.clicked += () =>
			{
				TotalActionCount = 0;
				actionScrollView?.Clear();
			};
			buttonContainer.Add(clearBtn);

			var purgeBtn = new Button();
			purgeBtn.text = "Purge";
			purgeBtn.clickable.clicked += () =>
			{
				Debug.Log("Purge Redux History");
				Purgers();

				//also clear the views of previous actions
				TotalActionCount = 0;
				actionScrollView?.Clear();

				//trigger explicit Garbage collection to boot
				GC.Collect();
			};
			buttonContainer.Add(purgeBtn);

			VisualElement labelFromUXML = _visualTreeAsset.Instantiate();
            rootVisualElement.Add(labelFromUXML);
        }

		private void AddOptionsView()
		{
			OptionsFoldout = GetFoldout("Options", rootVisualElement);

			if (OptionsFoldout == null)
			{
				Debug.LogError("Could not create Options Foldout!");
				return;
			}

			OptionsFoldout.style.flexShrink = 0;
			OptionsFoldout.style.borderBottomWidth = 1;
			OptionsFoldout.style.borderBottomColor = new Color(90f/255f, 90f / 255f, 90f / 255f);
			OptionsFoldout.style.marginBottom = 3;
			OptionsFoldout.style.paddingBottom = 3;
			AddAutoScrollToggle(OptionsFoldout);
			AddCollapseToggle(OptionsFoldout);

			var depthGroup = new VisualElement();
			depthGroup.style.flexDirection = FlexDirection.Row;
			depthGroup.style.flexShrink = 0;
			depthGroup.style.marginLeft = 3;
			depthGroup.style.marginRight = 3;
			OptionsFoldout.Add(depthGroup);

			var depthTitle = new Label("Max Depth");
			depthTitle.style.minWidth = LABEL_WIDTH;
			depthTitle.style.marginRight = 4;
			depthGroup.Add(depthTitle);

			DepthValueLabel = new Label(MaxDepth.ToString());
			DepthValueLabel.style.marginRight = 4;
			DepthValueLabel.text = MaxDepth.ToString();
			depthGroup.Add(DepthValueLabel);

			var slider = new SliderInt(2, 6);
			slider.RegisterValueChangedCallback(evt =>
			{
				DepthValueLabel.text = evt.newValue.ToString();
				MaxDepth = evt.newValue;
			});
			slider.value = MaxDepth;
			slider.style.flexGrow = 1;
			slider.style.maxWidth = 120;
			depthGroup?.Add(slider);

			FilterTitle = new Label("Filter Actions:");
			FilterTitle.style.minWidth = 135;
			FilterTitle.style.marginLeft = 3;
			FilterTitle.style.display = DisplayStyle.None;
			OptionsFoldout.Add(FilterTitle);
		}

		private void AddAutoScrollToggle(VisualElement parent)
		{
			AutoScroll = new Toggle("Auto Scroll");
			AutoScroll.labelElement.style.width = LABEL_WIDTH;

			bool? savedValue = null;
			if (PlayerPrefs.HasKey(PLAYER_PREFS_PREFIX + SCROLL_SETTING))
			{
				savedValue = PlayerPrefs.GetInt(PLAYER_PREFS_PREFIX + SCROLL_SETTING) == 1 ? true : false;
			}
			AutoScroll.value = savedValue != null ? savedValue.Value : true;

			AutoScroll.RegisterValueChangedCallback(evt =>
			{
				PlayerPrefs.SetInt(PLAYER_PREFS_PREFIX + SCROLL_SETTING, evt.newValue ? 1 : 0);
			});

			parent.Add(AutoScroll);
		}

		private void AddCollapseToggle(VisualElement parent)
		{
			CollapseToggle = new Toggle("Collapse Duplicate Actions");
			CollapseToggle.labelElement.style.width = LABEL_WIDTH;

			bool? savedValue = null;
			if (PlayerPrefs.HasKey(PLAYER_PREFS_PREFIX + COLLAPSE_SETTING))
			{
				savedValue = PlayerPrefs.GetInt(PLAYER_PREFS_PREFIX + COLLAPSE_SETTING) == 1 ? true : false;
			}
			CollapseToggle.value = savedValue != null ? savedValue.Value : true;

			CollapseToggle.RegisterValueChangedCallback(evt =>
			{
				PlayerPrefs.SetInt(PLAYER_PREFS_PREFIX + COLLAPSE_SETTING, evt.newValue ? 1 : 0);
			});

			parent.Add(CollapseToggle);
		}

		private void AddStoreFilterToggle(string storeName)
		{
			var storeToggle = new Toggle("Show actions for " + storeName);
			storeToggle.labelElement.style.width = LABEL_WIDTH;

			//get last saved setting for this toggle
			bool? savedValue = null;
			if (PlayerPrefs.HasKey(PLAYER_PREFS_PREFIX + FILTER_PREFIX + storeName))
			{
				savedValue = PlayerPrefs.GetInt(PLAYER_PREFS_PREFIX + FILTER_PREFIX + storeName) == 1 ? true : false;
			}
			storeToggle.value = savedValue!= null ? savedValue.Value : true;

			storeToggle.RegisterValueChangedCallback(evt =>
			{
				PlayerPrefs.SetInt(PLAYER_PREFS_PREFIX + FILTER_PREFIX + storeName, evt.newValue ? 1 : 0);
			});

			if (OptionsFoldout != null)
			{
				OptionsFoldout.Add(storeToggle);
			}

			if (StoreMap.ContainsKey(storeName))
			{
				StoreMap[storeName].StoreToggle = storeToggle;
			}

			if (FilterTitle != null)
			{
				FilterTitle.style.display = DisplayStyle.Flex;
			}
		}

		private void MakeStateView(VisualElement parent)
		{
			var stateElement = new VisualElement();
			stateElement.style.paddingBottom = 5;
			//stateElement.style.marginBottom = 5;
			//stateElement.style.borderBottomColor = Color.grey;
			//stateElement.style.borderBottomWidth = 2;
			parent.Add(stateElement);

			StateGroup = new ScrollView(ScrollViewMode.VerticalAndHorizontal);
			StateGroup.style.flexDirection = FlexDirection.ColumnReverse;
			StateGroup.style.flexGrow = 1;
			stateElement.Add(StateGroup);
		}

		public void DisplayStore<T>(ReduxStore<T> store) where T : class, new()
		{
			if(store == null)
			{
				Debug.LogError("The supplied store has not been initialized!");
				return;
			}

			//set up StoreMap entry to store various items
			var storeName = store.GetType().GenericTypeArguments[0].Name;

			if (StoreMap == null)
			{
				StoreMap = new Dictionary<string, ReduxStoreData>();
			}

			StoreMap.Add(storeName, new ReduxStoreData());

			TotalActionCount = 0;

			AddStoreFilterToggle(storeName);

			var storeHistoryAtInitialization = store.GetHistory();
			var historySub = store.ObserveHistory().StartWith(storeHistoryAtInitialization).Subscribe(historyInfos =>
			{
				//Debug.Log("HistorySize " + historyInfos.PreviousStates.Count);
				var mementosOrderedByDate = historyInfos.PreviousStates
						.OrderBy(reduxMemento => reduxMemento.Date)
						.ToList();

				var lastAction = mementosOrderedByDate.Last();
				var storeType = historyInfos.GetType().GenericTypeArguments[0].Name;
				MakeActionEntry( lastAction.Action, new System.Diagnostics.StackTrace(true), storeType);
			});
			Subscriptions.Add(historySub);

			var stateSub = store.Select().ThrottleFrame(1).Subscribe(state => {
				UpdateStateDisplay(state);
			});
			Subscriptions.Add(stateSub);

			Purgers += store.PurgeHistory;
		}

		private void MakeActionEntry(object action, System.Diagnostics.StackTrace stackTrace, string storeOfOrigin)
		{
			if(StoreMap.ContainsKey(storeOfOrigin))
			{
				if (StoreMap[storeOfOrigin].StoreToggle.value == false)
				{
					return;
				}
			}

			if(actionScrollView == null)
			{
				Debug.LogWarning($"UniRedux Inspector Not Ready...Ignoring Action {action}");
				return;
			}

			var inspectedObjects = new HashSet<object>();
			var depth = 0;

			TotalActionCount++;
			List<MemberInfo>? actionProperties = GetObjectProperties(action);
			var shouldCollapseDupes = CollapseToggle != null ? CollapseToggle.value : true;
			bool areActionsEqual = shouldCollapseDupes ? PublicInstancePropertiesEqual(LastAction, action, actionProperties) : false;
			if (!areActionsEqual)
			{
				Foldout? entry = GetFoldout($"[{TotalActionCount}] {action.GetType()}", actionScrollView);

				if (entry == null)
				{
					Debug.LogError("Problem creating foldout for action! Exiting...");
					return;
				}

				var stackText = new Label();
				stackText.text = stackTrace.ToString();

				Foldout? stackFoldout = GetFoldout("Stack Trace", entry);
				stackFoldout?.Add(stackText);
				entry.Add(stackFoldout);

				// display action properties
				TraverseObjectProperties(entry, action, depth, inspectedObjects, actionProperties);

				// add to scrollview
				actionScrollView?.Add(entry);   //add items to bottom of scroll
												//actionScrollView?.Insert(0, entry); // add items to top of scroll

				LastFoldout = entry;
				LastActionCount = 1;
			}
			else
			{
				if (LastFoldout != null)
				{
					// stop updating once we hit 1000
					if (LastActionCount < 1000)
					{
						// remove old number
						var foldoutTitle = LastFoldout.text.TrimStart($"{LastActionCount} ".ToCharArray());
						LastActionCount++;
						var plus = LastActionCount >= 1000 ? "+" : "";
						LastFoldout.text = $"{LastActionCount}{plus} {foldoutTitle}";
					}
				}
			}

			LastAction = action;

			if (AutoScroll != null && AutoScroll.value)
			{
				NeedsToScroll = true;
			}
		}

		private bool PublicInstancePropertiesEqual<T>(T? prevAction, T currAction, List<MemberInfo>? currProperties) where T : class
		{
			Type currType = currAction.GetType();

			//compare these properties to the previous action if it exists
			if (prevAction != null && currAction != null && currProperties != null)
			{
				Type prevType = prevAction.GetType();

				if (prevType == currType)
				{
					//Debug.Log($"ReduxDebug:: {currAction} has {currProperties.Count} properties to start.");
					foreach (System.Reflection.MemberInfo pi in currProperties)
					{

						object prevValue = GetValue(pi, prevAction);
						object currValue = GetValue(pi, currAction);

						if (prevValue != currValue && (prevValue == null || !prevValue.Equals(currValue)))
						{
							return false;
						}
					}
					return true;
				}
				return false;
			}

			return prevAction == currAction;
		}

		private void UpdateStateDisplay<T>(T state) where T : class, new()
		{
			var type = state.GetType();
			var stateName = type.Name;

			if (StoreMap.ContainsKey(stateName))
			{
				Foldout stateFoldout = StoreMap[stateName].StoreFoldout;

				if (stateFoldout == null)
				{
					stateFoldout = GetFoldout(stateName, StateGroup, true);
					stateFoldout.name = stateName;

					if (stateFoldout == null)
					{
						Debug.LogError("Problem creating foldout for state! Exiting...");
						return;
					}

					StoreMap[stateName].StoreFoldout = stateFoldout;
					stateFoldout.RegisterValueChangedCallback(evt =>
					{
						if (evt.newValue == true && evt.target == stateFoldout)
						{
							ApplyLastStateUpdate(stateFoldout);
						}
					});
				}

				if (stateFoldout.value) //only traverse if toggle is open and visible.
				{
					stateFoldout.Clear(); // is this going to be a problem? probably.

					int depth = 0;
					var inspectedObjects = new HashSet<object>();
					TraverseObjectProperties(stateFoldout, state, depth, inspectedObjects);
				}
				else
				{
					StoreMap[stateName].LastState = state;
				}
			}
		}

		private void ApplyLastStateUpdate(Foldout stateFoldout)
		{
			stateFoldout.Clear(); // is this going to be a problem? probably.

			int depth = 0;
			var inspectedObjects = new HashSet<object>();
			if (StoreMap.ContainsKey(stateFoldout.name))
			{
				TraverseObjectProperties(stateFoldout, StoreMap[stateFoldout.name].LastState, depth, inspectedObjects);
			}
			else
			{
				Debug.LogError("CouldNot apply last state!");
			}
		}

		// todo: this could maybe replace traverse object properties once i get it going
		private void TraverseObjectProperties(Foldout parent, object item, int depth, HashSet<object> inspectedObjects, List<MemberInfo>? itemProperties = null)
		{
			//todo: things to avoid
			//System.Type, System.Reflection
			depth++;

			if (itemProperties == null)
			{
				itemProperties = GetObjectProperties(item);
			}

			if (itemProperties != null)
			{
				//Debug.Log($"ReduxDebug:: {action} has {actionProperties.Count} properties.");
				foreach (MemberInfo prop in itemProperties)
				{
					if (depth <= MaxDepth)
					{
						object currValue = GetValue(prop, item, parent);

						if (IsPreviouslyInspected(currValue, inspectedObjects))
						{
							//Debug.Log($"ReduxDebug:: Duplicate Item Detected! {currValue}");
							var nameLabel = new Label($"Loop Detected - {prop.Name}");
							nameLabel.style.color = new Color(135f / 255f, 134f / 255f, 134f / 255f);
							parent.Add(nameLabel);
							parent.value = false;
							continue;
						}

						var inspectedObjectsClone = new HashSet<object>(inspectedObjects);
						AddToInspectedList(currValue, inspectedObjectsClone);
						AddPropertyDisplay(parent, prop, item, inspectedObjectsClone, depth);
					}
					else
					{
						var nameLabel = new Label($"Max Depth {MaxDepth} Reached");
						parent.Add(nameLabel);
						parent.value = false;
						break;  //b/c all the siblings will have the same depth.
					}
				}
			}
		}

		private void AddPropertyDisplay(Foldout parent, MemberInfo property, object item, HashSet<object> inspectedObjects, int depth)
		{
			object currValue = GetValue(property, item, parent);
			var underlyingType = GetUnderlyingType(property);

			if (UpdateDisplayForSimpleTypes(parent, underlyingType, currValue, property.Name))
			{
				return;
			}

			Foldout? typeFoldout = GetFoldout(property.Name, parent, true);

			if (typeFoldout == null)
			{
				Debug.LogError("Unable to create foldout for " + property.Name);
				return;
			}

			var isCollection = IsPropertyACollection(currValue);
			if (isCollection)
			{
				var currAsList = currValue as IEnumerable;
				if (currAsList != null)
				{
					var index = -1;
					foreach (var lval in currAsList)
					{
						index++;

						if (lval != null)
						{
							// display for dictionaries
							var listItemType = lval.GetType();
							if (listItemType.Name.Contains("KeyValuePair"))
							{
								var lvalProps = GetObjectProperties(lval);
								if (lvalProps != null && lvalProps.Count >= 2)
								{
									var key = GetValue(lvalProps[0], lval);
									var value = GetValue(lvalProps[1], lval);

									var kvpFoldout = GetFoldout($"{key} ", typeFoldout);
									if (kvpFoldout != null)
									{
										TraverseObjectProperties(kvpFoldout, value, depth, inspectedObjects);
									}
								}

								continue;
							}

							// display for primitive types & strings
							if (UpdateDisplayForSimpleTypes(typeFoldout, listItemType, lval, $"{listItemType.Name} [{index}]"))
							{
								continue;
							}

							// display for more complex types
							var listObjFoldout = GetFoldout($"{listItemType.Name} [{index}]", typeFoldout);
							if (listObjFoldout != null)
							{
								TraverseObjectProperties(listObjFoldout, lval, depth, inspectedObjects);
							}
						}
						else
						{
							//todo: see if there's a way to deduce type of this null thing
						}
					}

					if (index == -1)	//if nothing was in the list
					{
						typeFoldout.value = false;
					}
				}
				return;
			}

			// everything past this point is a custom type
			TraverseObjectProperties(typeFoldout, currValue, depth, inspectedObjects);

			//Debug.Log($"ReduxDebug:: Creating Foldout for {underlyingType.Name} with {properties?.Count} additional properties.");
		}

		private bool IsPreviouslyInspected(object? item, HashSet<object> inspectedObjects)
		{
			//do i need this upper part? the object is either in there or not.
			if (item == null)
			{
				return false;
			}
			var currType = item.GetType();
			var nullable = IsNullable(currType);

			var isPrimitiveNullable = nullable && currType.IsPrimitive;
			if (currType.IsPrimitive || isPrimitiveNullable)
			{ 
				return false;
			}

			//Debug.Log($"ReduxDebug:: Previously Inspected test on {item} - Type {currType}");
			return inspectedObjects.Contains(item);
		}

		private void AddToInspectedList(object item, HashSet<object> inspectedObjects)
		{
			//don't add null to list
			if (item == null)
			{
				return;
			}

			var currType = item.GetType();
			var nullable = IsNullable(currType);

			//don't add primitive types or strings to list
			var isPrimitiveNullable = nullable && currType.IsPrimitive;
			if (currType.IsPrimitive || isPrimitiveNullable || ObjectOverridesToString(item))
			{
				return;
			}

			inspectedObjects.Add(item);
		}

		private bool UpdateDisplayForSimpleTypes(Foldout parent, Type type, object currValue, string propertyName)
		{
			var overridesToString = ObjectOverridesToString(currValue);

			// ** had to include currValue == null here b/c some types will not report as nullable even when they are.
			var nullable = currValue == null || IsNullable(type);
			//Debug.Log($"ReduxDebug:: {propertyName} - Type {type}");

			var showSimpleNullable = nullable && (currValue == null || currValue.GetType().IsPrimitive);
			if (type.IsPrimitive || overridesToString || showSimpleNullable)
			{
				var row = GetPropertyDisplay(propertyName, currValue != null ? currValue.ToString() : "null");
				parent.Add(row);
				return true;
			}

			return false;
		}

		private Foldout? GetFoldout(string name, VisualElement? parent, bool isOpen = false)
		{
			if (parent == null)
			{
				Debug.LogError("Could not create foldout. Parent provided was null!");
				return null;
			}

			Foldout foldout = new Foldout();
			foldout.text = name;
			foldout.contentContainer.style.paddingLeft = 15;
			foldout.value = isOpen;

			var foldoutLabel = foldout.Q<Toggle>()?.Q<Label>();
			if(foldoutLabel != null)
			{
				foldoutLabel.RegisterCallback<PointerOutEvent>((e) =>
				{
					foldoutLabel.style.backgroundColor = Color.clear;
				});
				foldoutLabel.RegisterCallback<PointerOverEvent>((e) =>
				{
					foldoutLabel.style.backgroundColor = HighlightColor;
				});
			}

			parent?.Add(foldout);
			return foldout;
		}

		private VisualElement GetPropertyDisplay(string name, string? value)
		{
			VisualElement row = new VisualElement();
			row.style.width = Length.Percent(100);
			row.style.flexDirection = FlexDirection.Row;
			row.RegisterCallback<PointerOutEvent>((e) =>
			{
				row.style.backgroundColor = Color.clear;
			});
			row.RegisterCallback<PointerOverEvent>((e) =>
			{
				row.style.backgroundColor = HighlightColor;
			});

			var nameLabel = new Label();
			nameLabel.text = name;
			nameLabel.style.width = LABEL_WIDTH;
			nameLabel.style.maxWidth = 300;
			nameLabel.style.minWidth = 100;
			row.Add(nameLabel);

			if (value != null)
			{
				var valueLabel = new Label();
				valueLabel.text = value;
				valueLabel.style.unityFontStyleAndWeight = FontStyle.Bold;
				row.Add(valueLabel);
			}

			return row;
		}

		private List<MemberInfo>? GetObjectProperties(object obj)
		{
			if(obj == null)
			{
				return null;
			}

			Type currType = obj.GetType();

			MemberInfo[] properties = currType.GetProperties(System.Reflection.BindingFlags.Public | System.Reflection.BindingFlags.Instance);
			MemberInfo[] fields = currType.GetFields();

			var currProperties = new List<MemberInfo>(properties);
			currProperties.AddRange(fields);

			return currProperties;
		}

		// gets the value of memberInfos derived from Action type
		private object GetValue(MemberInfo memberInfo, object forObject, VisualElement? parent = null)
		{
			try
			{
				switch (memberInfo.MemberType)
				{
					case MemberTypes.Field:
						return ((FieldInfo)memberInfo).GetValue(forObject);
					case MemberTypes.Property:
						return ((PropertyInfo)memberInfo).GetValue(forObject);
					default:
						throw new NotImplementedException();
				}
			}
			catch (Exception e)
			{
				//Debug.LogError($"[{TotalActionCount}] {memberInfo.MemberType}  / { memberInfo.DeclaringType} /  {memberInfo.Name}");
				return e.ToString();
			}
		}

		private Type GetUnderlyingType(MemberInfo member)
		{
			switch (member.MemberType)
			{
				case MemberTypes.Event:
					return ((EventInfo)member).EventHandlerType;
				case MemberTypes.Field:
					return ((FieldInfo)member).FieldType;
				case MemberTypes.Method:
					return ((MethodInfo)member).ReturnType;
				case MemberTypes.Property:
					return ((PropertyInfo)member).PropertyType;
				default:
					throw new ArgumentException
					(
					 "Input MemberInfo must be if type EventInfo, FieldInfo, MethodInfo, or PropertyInfo"
					);
			}
		}

		private bool ObjectOverridesToString(object currValue)
		{
			return currValue != null ? currValue.ToString() != currValue.GetType().ToString() : false;
		}

		private bool IsPropertyACollection(object obj)
		{
			var type = obj.GetType();
			TypeInfo typeInfo = type.GetTypeInfo();

			if (typeInfo.IsArray == true)
				return true;

			if (typeof(System.Collections.IList).IsAssignableFrom(type) == true)
				return true;

			if (typeof(System.Collections.IDictionary).IsAssignableFrom(type) == true)
				return true;

			return false;
			
		}

		static bool IsNullable(Type type)
		{
			return type.IsGenericType && (type.GetGenericTypeDefinition() == typeof(Nullable<>));
		}

		private void Update()
		{
			if (NeedsToScroll)
			{
				UpdateScollPosition();
				NeedsToScroll = false;
			}
		}

		private void UpdateScollPosition()
		{
			if (actionScrollView != null)
			{
				actionScrollView.scrollOffset = actionScrollView.contentContainer.layout.max - actionScrollView.contentViewport.layout.size;
			}
		}

		private void OnDestroy()
		{
			Debug.Log("ReduxInspector Destroyed.");
			foreach(var sub in Subscriptions)
			{
				sub.Dispose();
			}
		}
	}
}