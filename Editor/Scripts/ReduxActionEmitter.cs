using UnityEditor;
using UnityEngine;
using UnityEngine.UIElements;
using UniRedux;
using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Data.SqlTypes;

namespace UniRedux.Tools
{
	public class AddToListButton : Button
	{
		public int Count { private set; get; }
		public Action<int>? ClickCallback;

		public AddToListButton()
		{
			this.clicked += OnClick;
		}

		public void OnClick()
		{
			ClickCallback?.Invoke(Count);
			Count++;
		}
	}

	public class ActionFieldInfo
	{
		public Type Type;
		public VisualElement Element;
		public MemberInfo MemberInfo;
		public object Instance;
		public bool IsListItem;
		public bool IsNull;

		public ActionFieldInfo(MemberInfo mi, VisualElement ve, bool listItem = false)
		{
			Element = ve;
			MemberInfo = mi;
			Type = ReduxActionEmitter.GetUnderlyingType(mi);
			IsListItem = listItem;
		}

		public ActionFieldInfo(MemberInfo mi, Type type, VisualElement ve, bool listItem = false)
		{
			Element = ve;
			MemberInfo = mi;
			Type = type;
			IsListItem = listItem;
		}
	}

	public class ReduxActionEmitter : EditorWindow
	{
		[SerializeField]
		private VisualTreeAsset _visualTreeAsset = default;
		private static ReduxActionEmitter? m_Instance;
		private ScrollView? actionScrollView;
		private Foldout? OptionsFoldout;
		private const string PLAYER_PREFS_PREFIX = "ReduxActionEmitter";
		private const int LABEL_WIDTH = 130;
		private Type StringType = typeof(String);
		private Color DefaultLabelColor = new Color(210f / 255f, 210f / 255f, 210f / 255f);
		private Color DisabledLabelColor = new Color(80f / 255f, 80f / 255f, 80f / 255f);
		private Dictionary<string, List<string>> ActionLookup = new Dictionary<string, List<string>>();
		private Dictionary<string, ActionFieldInfo> FieldLookup = new Dictionary<string, ActionFieldInfo>();
		private List<StoreData> KnownStores = new List<StoreData>();
		private const string SEPARATOR = ".";
		private bool IsReady;
		private List<Assembly> PendingAssemblies;

		private struct StoreData
		{
			public IReduxStore Store;
			public string DisplayName;
		}

		public void OnEnable()
		{
			IsReady = false;
		}

		public static ReduxActionEmitter? Instance
		{
			get
			{
				if (m_Instance == null)
				{
					return FindFirstInstance();
				}

				return m_Instance;
			}
		}

		public static ReduxActionEmitter? FindFirstInstance()
		{
			var windows = (ReduxActionEmitter[])Resources.FindObjectsOfTypeAll(typeof(ReduxActionEmitter));
			if (windows.Length == 0)
				return null;
			return windows[0];
		}

		[MenuItem("DropFake/UniRedux Tools/UniRedux Action Emitter")]
		public static void ShowInspector()
		{
			ReduxActionEmitter wnd = GetWindow<ReduxActionEmitter>();
			m_Instance = wnd;
			wnd.titleContent = new GUIContent("UniRedux Action Emitter");
		}

		public void CreateGUI()
		{
			ActionLookup.Clear();
			FieldLookup.Clear();
			//AddOptionsView();

			actionScrollView = new ScrollView(ScrollViewMode.VerticalAndHorizontal);
			actionScrollView.style.flexGrow = 1;
			actionScrollView.style.paddingBottom = 10;
			rootVisualElement.Add(actionScrollView);

			VisualElement labelFromUXML = _visualTreeAsset.Instantiate();
			rootVisualElement.Add(labelFromUXML);
			IsReady = true;

			ProcessPendingAssemblies();
		}

		private void AddOptionsView()
		{
			OptionsFoldout = GetFoldout("Options", rootVisualElement);

			if (OptionsFoldout == null)
			{
				Debug.LogError("Could not create Options Foldout!");
				return;
			}

			OptionsFoldout.style.flexShrink = 0;
			OptionsFoldout.style.borderBottomWidth = 1;
			OptionsFoldout.style.borderBottomColor = new Color(90f / 255f, 90f / 255f, 90f / 255f);
			OptionsFoldout.style.marginBottom = 3;
			OptionsFoldout.style.paddingBottom = 3;
		}

		private void ProcessPendingAssemblies()
		{
			if (PendingAssemblies != null)
			{
				foreach (var assembly in PendingAssemblies)
				{
					DetectActionsInAssembly(assembly);
				}
			}

			PendingAssemblies = null;
		}

		public void DetectActionsInAssembly(Assembly assembly)
		{
			if (!IsReady)
			{
				if (PendingAssemblies == null)
				{
					PendingAssemblies = new List<Assembly>();
				}
				PendingAssemblies.Add(assembly);
				return;
			}

			foreach (Type type in assembly.GetTypes())
			{
				var isAction = typeof(IReduxAction).IsAssignableFrom(type);
				if (isAction)
				{
					//Debug.Log($"Redux Action Emitter found the following Action: {type.FullName}");
					MakeActionItem(type);
				}
			}
		}

		public void RegisterStore(string name, IReduxStore store)
		{
			//detect duplicates
			foreach (var storeItem in KnownStores)
			{
				if (storeItem.Store == store)
				{
					return;
				}
			}

			StoreData data = new StoreData();
			data.Store = store;
			data.DisplayName = name;
			KnownStores.Add(data);
		}

		private void MakeActionItem(Type type)
		{
			var actionName = type.Name;
			var foldout = GetFoldout(actionName, actionScrollView);
			foldout.style.marginRight = 10;
			foldout.style.marginLeft = 10;
			foldout.style.marginTop = 10;
			foldout.name = actionName;
			foldout.style.borderBottomColor = Color.black;
			foldout.style.borderBottomWidth = 2;
			foldout.style.borderTopColor = Color.black;
			foldout.style.borderTopWidth = 2;
			foldout.style.borderLeftColor = Color.black;
			foldout.style.borderLeftWidth = 2;
			foldout.style.borderRightColor = Color.black;
			foldout.style.borderRightWidth = 2;

			ActionLookup.Add(actionName, new List<string>());

			var block = new VisualElement();
			block.name = "Block";
			block.style.paddingBottom = 5;
			block.style.paddingTop = 5;
			block.style.paddingRight = 5;

			TraverseTypeProperties(block, type, actionName, actionName);

			var spacer = new VisualElement();
			spacer.name = "Spacer";
			spacer.style.height = 5;
			block.Add(spacer);

			block.Add(GetStoreSelector());
			var sendButton = new Button();
			sendButton.text = "Send";
			sendButton.style.marginLeft = 0;
			sendButton.style.marginRight = 0;
			sendButton.clicked += () =>
			{
				SendAction(type, foldout);
			};
			block.Add(sendButton);

			foldout.Add(block);
		}

		private VisualElement GetStoreSelector()
		{
			var dropdown = new DropdownField("Destination");
			dropdown.name = "StoreSelector";
			dropdown.choices = KnownStores.Select(s => s.DisplayName).ToList();
			dropdown.index = 0;
			dropdown.style.marginLeft = 0;
			dropdown.style.marginRight = 0;
			dropdown.labelElement.style.minWidth = LABEL_WIDTH;
			return dropdown;
		}

		private void SendAction(Type type, VisualElement item)
		{
			var actionName = type.Name;

			try
			{
				var action = Activator.CreateInstance(type);
				var itemProperties = GetTypeProperties(type);

				var actionProperties = ActionLookup[actionName];
				foreach (var propName in actionProperties)
				{
					//Debug.Log($"processing {actionName} property: {propName}");
					SetValueOnAction(action, propName, item, type);
				}

				Debug.Log("Sending " + type);

				//get which store it needs to be sent to
				var storeSelection = item.Q("StoreSelector") as DropdownField;
				KnownStores[storeSelection.index].Store.Dispatch(action);
			}
			catch (Exception e)
			{
				Debug.LogError(e.Message);
			}
		}

		private void SetValueOnAction(object action, string propName, VisualElement parent, Type actionType)
		{
			if (!FieldLookup.TryGetValue(propName, out ActionFieldInfo actionFieldInfo))
			{
				Debug.LogError("Could not find stored Field Info for " + propName);
				return;
			}

			//strip action name from propName -- that was only added to make sure each name was unique
			var separatorIdx = propName.IndexOf(SEPARATOR);
			var strippedPropName = propName;
			if (separatorIdx > -1)
			{
				strippedPropName = propName.Substring(separatorIdx + 1);
			}

			var lineage = strippedPropName.Split(SEPARATOR);
			VisualElement element = parent;
			foreach (var child in lineage)
			{
				element = element.Q(child);
				if (element == null)
				{
					Debug.LogError($"{strippedPropName} could not find visual element {child} on parent {element}!");
				}
			}

			Type underlyingType = Nullable.GetUnderlyingType(actionFieldInfo.Type)?? actionFieldInfo.Type;

			object target = action;
			if (!HasMember(actionType, strippedPropName))
			{
				//look for a different target
				var idx = propName.LastIndexOf(SEPARATOR);

				if (idx == -1)
				{
					Debug.LogError($"Could not find ancestor or direct target for {propName}");
					return;
				}

				var parentName = propName.Substring(0, idx);
				if (!FieldLookup.TryGetValue(parentName, out ActionFieldInfo parentFieldInfo))
				{
					Debug.LogError("Could not find stored parent Field Info for " + parentName);
					return;
				}

				if (parentFieldInfo.Instance == null)
				{
					Debug.LogError("Error finding ParentFieldInfo Instance.");
					return;
				}

				target = parentFieldInfo.Instance;
			}

			if (element is TextField)
			{
				var value = ((TextField)element).value; // get the value

				if (actionFieldInfo.IsListItem)
				{
					(target as IList).Add(Convert.ChangeType(value, underlyingType));
					return;
				}

				var nullable = IsNullable(actionFieldInfo.Type);
				if (nullable)
				{
					//Type t = Nullable.GetUnderlyingType(actionFieldInfo.Type);
					object safeValue = actionFieldInfo.IsNull ? null : Convert.ChangeType(value, underlyingType);

					SetValue(actionFieldInfo.MemberInfo, target, safeValue);
				}
				else
				{
					SetValue(actionFieldInfo.MemberInfo, target, Convert.ChangeType(value, actionFieldInfo.Type));
				}
			}
			else if(element is Toggle)
			{
				var value = ((Toggle)element).value;

				if (actionFieldInfo.IsListItem)
				{
					(target as IList).Add(value);
					return;
				}

				SetValue(actionFieldInfo.MemberInfo, target, actionFieldInfo.IsNull? null : value);
			}
			else if (element is DropdownField)
			{
				if (underlyingType.IsEnum)
				{
					var selectedIndex = ((DropdownField)element).index;
					var value = Enum.GetValues(underlyingType).GetValue(selectedIndex);

					if (actionFieldInfo.IsListItem)
					{
						if (actionFieldInfo.IsNull)
						{
							var index = (target as IList).Add(Activator.CreateInstance(actionFieldInfo.Type)); // does this work?
							//SetValue(actionFieldInfo.MemberInfo, (target as IList)[index], null);
							return;
						}

						(target as IList).Add(actionFieldInfo.IsNull ? null : value);
						return;
					}

					SetValue(actionFieldInfo.MemberInfo, target, actionFieldInfo.IsNull ? null : value);
				}
			}
			else
			{
				//Debug.Log($"Found unknown type for processing...creating instance of {actionFieldInfo.Type}");

				object value;
				if (actionFieldInfo.Type.IsInterface)
				{
					//TODO: kind of jank IReadOnlyList detection, replace this with something better
					if (actionFieldInfo.Type.Name.Contains("IReadOnlyList"))
					{
						var genericArgs = actionFieldInfo.Type.GetGenericArguments();
						var arg = genericArgs[0];
						Type unboundListType = typeof(List<>);
						Type constructed = unboundListType.MakeGenericType(arg);
						value = Activator.CreateInstance(constructed);
					}
					else
					{
						throw new Exception("This type is not yet supported by ReduxActionEmitter.");
					}
				}
				else if (actionFieldInfo.Type.IsGenericType)
				{
					Type[] typeArgs = { GetCollectedType(actionFieldInfo.MemberInfo) };
					var genTypeDef = actionFieldInfo.Type.GetGenericTypeDefinition();
					var genericType = genTypeDef.MakeGenericType(typeArgs);
					value = Activator.CreateInstance(genericType);
				}
				else
				{
					value = Activator.CreateInstance(actionFieldInfo.Type);
				}

				actionFieldInfo.Instance = value; // store this here so that we can populate it with further properties if necessary.

				if (actionFieldInfo.IsListItem)
				{
					(target as IList).Add(value);
					return;
				}

				if (actionFieldInfo.IsNull)
				{
					SetValue(actionFieldInfo.MemberInfo, target, null);
					return;
				}

				SetValue(actionFieldInfo.MemberInfo, target, value);
			}
		}

		private void SetValue(MemberInfo memberInfo, object action, object value)
		{
			if (memberInfo.MemberType == MemberTypes.Field)
			{
				(memberInfo as FieldInfo).SetValue(action, value);
			}
			else if (memberInfo.MemberType == MemberTypes.Property)
			{
				(memberInfo as PropertyInfo).SetValue(action, value);
			}
		}

		private bool HasMember(Type type, string memberName)
		{
			if (type.GetFields().Any(f => f.Name == memberName))
			{
				return true;
			}

			if (type.GetProperties().Any(p => p.Name == memberName))
			{
				return true;
			}

			return false;
		}

		private void StoreLookupData(string actionName, string fullname, MemberInfo info, VisualElement parent)
		{
			//Debug.Log($"Storing Field Info for {fullname}");
			try
			{
				ActionLookup[actionName].Add(fullname);
				FieldLookup.Add(fullname, new ActionFieldInfo(info, parent));
			}
			catch(Exception e)
			{
				Debug.LogError(e.Message);
			}
		}

		private void StoreListItemLookupData(string actionName, string fullname, Type type, VisualElement parent)
		{
			//Debug.Log($"Storing Field Info for {fullname}");
			try
			{
				ActionLookup[actionName].Add(fullname);
				FieldLookup.Add(fullname, new ActionFieldInfo(null, type, parent, true));
			}
			catch (Exception e)
			{
				Debug.LogError(e.Message);
			}
		}

		private void TraverseTypeProperties(VisualElement parent, Type type, String actionName, string lineageName = "")
		{
			var itemProperties = GetTypeProperties(type);
			if (itemProperties != null)
			{
				//Debug.Log($"ReduxDebug:: {action} has {actionProperties.Count} properties.");
				foreach (MemberInfo prop in itemProperties)
				{
					AddPropertyDisplay(parent, prop, actionName, lineageName);
				}
			}
		}

		private void AddPropertyDisplay(VisualElement parent, MemberInfo property, string actionName, string lineageName = "")
		{
			var separator = String.IsNullOrEmpty(lineageName) ? "" : SEPARATOR;
			var fullname = $"{lineageName}{separator}{property.Name}";

			var underlyingType = GetUnderlyingType(property);
			StoreLookupData(actionName, fullname, property, parent);

			//Debug.Log($"Adding ActionFieldInfo: {fullname} -> {property.Name}, {underlyingType}");

			if (HandleSimpleTypeDisplay(parent, underlyingType, property.Name, property.Name, fullname) != null)
			{
				return;
			}

			var isCollection = IsCollection(property);
			if (isCollection)
			{
				var collectedType = GetCollectedType(property);
				var addItemButton = new AddToListButton();
				addItemButton.text = " + ";
				addItemButton.style.marginLeft = 0;
				addItemButton.style.marginRight = 0;
				var collectionFoldout = GetFoldout(property.Name, parent, true);
				collectionFoldout.contentContainer.style.paddingLeft = 0;
				collectionFoldout.contentContainer.style.marginLeft = 0;
				collectionFoldout.Add(addItemButton);
				AddNullOption(collectionFoldout, fullname);
				addItemButton.ClickCallback = (int index) =>
				{
					AddListItem(collectedType, property, collectionFoldout, actionName, fullname, index);
				};
				return;
			}

			//if it's not a simple type create a block and label and then dig in
			var typeFoldOut = GetFoldout(property.Name, parent, true);
			//fullname = $"{lineageName}{separator}{underlyingType.Name}"; //why did i do this?? instead of keeping prev name?
			TraverseTypeProperties(typeFoldOut, underlyingType, actionName, fullname); //TODO: should this be lineageName + underlying type??
			AddNullOption(typeFoldOut, fullname);
		}

		private void AddNullOption(VisualElement parent, string fullName)
		{
			var destination = parent;
			if (parent is Foldout) // this is a little hacky and subject to break in future versions. 
			{
				var toggle = parent.Q<Toggle>();
				if (toggle != null)
				{
					destination = toggle;
				}
			}

			var nullToggle = new Toggle("Null?");
			nullToggle.style.marginLeft = 0;
			nullToggle.labelElement.style.color = DisabledLabelColor;
			nullToggle.labelElement.style.paddingLeft = 2;
			nullToggle.labelElement.style.minWidth = 40;
			destination.Add(nullToggle);

			nullToggle.RegisterValueChangedCallback((e) =>
			{
				//Debug.Log($"Set to null is {e.newValue} target {e.target}");

				if (FieldLookup.ContainsKey(fullName))
				{
					FieldLookup[fullName].IsNull = e.newValue;
				}
				else
				{
					Debug.LogError("could not find Field Info for " + fullName);
				}

				//visually update siblings to specify that those values will not be set
				var siblings = nullToggle.parent.Children();
				foreach (var sibling in siblings)
				{
					if (sibling == nullToggle)
					{
						continue;
					}
					var label = sibling.Q<Label>();
					if (label != null)
					{
						label.style.color = e.newValue? DisabledLabelColor : DefaultLabelColor;
					}
				}
				nullToggle.labelElement.style.color = e.newValue ? DefaultLabelColor : DisabledLabelColor;

				if (parent is Foldout)
				{
					(parent as Foldout).value = e.newValue ? false : true;	// toggle foldout parent open / closed based on null status
				}
			});
		}

		private Type GetCollectedType(MemberInfo property)
		{
			if (property is FieldInfo)
			{
				var args = (property as FieldInfo)?.FieldType?.GetGenericArguments();
				if (args != null && args.Length > 0)
				{
					return args[0];
				}
			}

			if (property is PropertyInfo)
			{
				var args = (property as PropertyInfo)?.PropertyType?.GetGenericArguments();
				if (args != null && args.Length > 0)
				{
					return args[0];
				}
			}

			return null;
		}

		private void AddListItem(Type listType, MemberInfo info, VisualElement parent, string actionName, string lineageName, int index)
		{
			var fullName = lineageName + SEPARATOR + listType.Name + index;
			var listItemName = $"{listType.Name}{index}";
			var listItemDisplayName = $"{listType.Name} [{index}]";

			if (IsNullable(listType))
			{
				listItemDisplayName = $"{Nullable.GetUnderlyingType(listType).Name} [{index}]"; 
			}

			var listItemRow = HandleSimpleTypeDisplay(parent, listType, listItemDisplayName, listItemName, fullName);
			if (listItemRow != null)
			{
				StoreListItemLookupData(actionName, fullName, listType, listItemRow);

				AddDeleteOption(listItemRow, fullName, actionName);

				//Debug.Log($"Adding ActionFieldInfo: {fullname} -> {listType.Name}, {listType}");
				return;
			}

			var listItemFoldout = GetFoldout(listItemName, parent, true, listItemDisplayName);
			StoreListItemLookupData(actionName, fullName, listType, listItemFoldout);
			AddDeleteOption(listItemFoldout, fullName, actionName);

			//Debug.Log($"2 Adding ActionFieldInfo: {fullname} -> {listItemName}, {listType}");
			TraverseTypeProperties(listItemFoldout, listType, actionName, fullName);
		}

		private void AddDeleteOption(VisualElement parent, string fullName, string actionName)
		{
			var destination = parent;
			if (parent is Foldout) // this is a little hacky and subject to break in future versions. 
			{
				var toggle = parent.Q<Toggle>();
				if (toggle != null)
				{
					destination = toggle;
				}
			}

			var deleteButton = new Button();
			deleteButton.text = "-";
			deleteButton.style.marginLeft = 0;
			deleteButton.style.marginRight = 0;
			deleteButton.style.paddingRight = 6;
			deleteButton.style.paddingLeft = 6;
			deleteButton.clicked += () =>
			{
				DeleteListItem(fullName, actionName);
			};

			destination.Add(deleteButton);
		}

		private void DeleteListItem(string itemName, string actionName)
		{
			try
			{
				if (ActionLookup.ContainsKey(actionName))
				{
					//remove item and decendents from ActionLookup
					var actionProps = ActionLookup[actionName];
					for (var i = actionProps.Count - 1; i >= 0; i--)
					{
						if (actionProps[i].StartsWith(itemName))
						{
							actionProps.RemoveAt(i);
						}
					}

					if (FieldLookup.ContainsKey(itemName))
					{
						var fieldInfo = FieldLookup[itemName];
						fieldInfo.Element.parent.Remove(fieldInfo.Element);
						FieldLookup.Remove(itemName);
					}
					else
					{
						Debug.LogError("Could not find Action Field Info for " + itemName);
					}
				}
				else
				{
					Debug.LogError("Could not find action " + actionName);
				}

			}
			catch(Exception e)
			{
				Debug.LogError(e.Message);
			}
		}


		private VisualElement HandleSimpleTypeDisplay(VisualElement parent, Type type, string displayName, string propertyName, string fullname)
		{
			var nullable = IsNullable(type);
			//Debug.Log($"ReduxDebug:: {propertyName} - Type {type}, Nullable? {nullable}");

			//todo: should it go in here if it's nullable? or only if it's primitive AND nullable?
			if (type.IsPrimitive || type == StringType || nullable || type.IsEnum)
			{
				var row = GetPropertyDisplay(displayName, propertyName, type, nullable, fullname);
				parent.Add(row);
				return row;
			}

			return null;
		}

		private Foldout? GetFoldout(string name, VisualElement? parent, bool isOpen = false, string displayName = "")
		{
			if (parent == null)
			{
				Debug.LogError($"Could not create foldout {name}. Parent provided was null!");
				return null;
			}

			Foldout foldout = new Foldout();
			foldout.text = !String.IsNullOrEmpty(displayName)? displayName : name;
			foldout.name = name;
			foldout.contentContainer.style.paddingLeft = 15;
			foldout.value = isOpen;

			parent?.Add(foldout);
			return foldout;
		}

		private VisualElement GetPropertyDisplay(string displayName, string name, Type type, bool isNullable, string fullName)
		{
			VisualElement row = new VisualElement();
			row.name = fullName;
			row.style.width = Length.Percent(100);
			row.style.flexDirection = FlexDirection.Row;

			//Debug.Log($"Get Property Display {type}");
			if (isNullable)
			{
				type = Nullable.GetUnderlyingType(type);
			}

			if (type == typeof(bool))
			{
				var toggle = new Toggle(displayName);
				toggle.name = name;
				toggle.style.marginLeft = 0;
				//toggle.style.marginRight = 0;
				toggle.labelElement.style.minWidth = LABEL_WIDTH;
				row.Add(toggle);
			}
			else if (type.IsEnum)
			{
				//Debug.Log("ENUM fullname" + fullName);
				var dropdown = new DropdownField(displayName);
				dropdown.choices = Enum.GetNames(type).ToList();
				dropdown.name = name;
				dropdown.labelElement.style.minWidth = LABEL_WIDTH;
				dropdown.style.marginLeft = 0;
				dropdown.style.marginRight = 0;
				dropdown.index = 0;
				row.Add(dropdown);
			}
			else //(type == StringType)
			{
				var nameLabel = new Label();
				nameLabel.text = displayName;
				nameLabel.style.width = LABEL_WIDTH;
				nameLabel.style.maxWidth = 300;
				nameLabel.style.minWidth = 100;
				nameLabel.style.marginRight = 2;

				row.Add(nameLabel);

				var valueLabel = new TextField();
				valueLabel.style.flexGrow = 1;
				valueLabel.style.marginRight = 2;
				valueLabel.style.marginLeft = 0;
				valueLabel.name = name;
				valueLabel.multiline = false; //(type != StringType) ? false : true;
				row.Add(valueLabel);
			}

			if (isNullable)
			{
				AddNullOption(row, fullName);
			}

			return row;
		}

		private List<MemberInfo>? GetTypeProperties(Type currType)
		{
			MemberInfo[] properties = currType.GetProperties(System.Reflection.BindingFlags.Public | System.Reflection.BindingFlags.Instance);
			MemberInfo[] fields = currType.GetFields();

			var currProperties = new List<MemberInfo>(properties);
			currProperties.AddRange(fields);

			return currProperties;
		}

		public static Type GetUnderlyingType(MemberInfo member)
		{
			switch (member.MemberType)
			{
				case MemberTypes.Event:
					return ((EventInfo)member).EventHandlerType;
				case MemberTypes.Field:
					return ((FieldInfo)member).FieldType;
				case MemberTypes.Method:
					return ((MethodInfo)member).ReturnType;
				case MemberTypes.Property:
					return ((PropertyInfo)member).PropertyType;
				default:
					throw new ArgumentException
					(
					 "Input MemberInfo must be if type EventInfo, FieldInfo, MethodInfo, or PropertyInfo"
					);
			}
		}

		private bool IsCollection(MemberInfo info)
		{
			Type type;
			if (info.MemberType == MemberTypes.Property)
			{
				type = (info as PropertyInfo).PropertyType;
			}
			else if (info.MemberType == MemberTypes.Field)
			{
				type = (info as FieldInfo).FieldType;
			}
			else
			{
				type = GetType();
			}

			if (info.ReflectedType.IsArray == true)
				return true;

			if (typeof(System.Collections.IList).IsAssignableFrom(type) == true)
				return true;

			if (typeof(System.Collections.IDictionary).IsAssignableFrom(type) == true)
				return true;

			if (typeof(System.Collections.IEnumerable).IsAssignableFrom(type) == true)
				return true;

			if (type.FullName.Contains("IReadOnlyList"))
			{
				return true;
			}
			return false;
		}

		static bool IsNullable(Type type)
		{
			return type.IsGenericType && (type.GetGenericTypeDefinition() == typeof(Nullable<>));
		}

		private void OnDestroy()
		{
			IsReady = false;
		}
	}
}