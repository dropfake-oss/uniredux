# uniredux

Port of https://github.com/Odonno/ReduxSimple configured for Unity and using UniRx as the base instead of System.Reactive

## ManagerWithStore

A simple example of using ManagerWithStore with a class and state would be:

```
	public class SimpleState
	{
		public GameState? State { get; set; } = null;

		public static SimpleState InitialState =>
			new SimpleState
			{
				State = new GameState(),
			};
	}

	public class SimpleManager : ManagerWithStore<SimpleManager, SimpleState>
	{
		public static void Initialize()
		{
			Initialize(GetReducers(), SimpleState.InitialState);
		}
	}

```
