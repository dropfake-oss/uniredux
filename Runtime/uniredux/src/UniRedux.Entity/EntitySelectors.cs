﻿using System;
using System.Collections.Generic;
using System.Linq;
using UniRx;
using static UniRedux.Selectors;

namespace UniRedux.Entity
{
    /// <summary>
    /// Collection of selectors for an <see cref="EntityState{TKey, TEntity}"/>.
    /// </summary>
    /// <typeparam name="TKey">Primary key of the entity.</typeparam>
    /// <typeparam name="TEntity">Type of the entity.</typeparam>
    public class EntitySelectors<TKey, TEntity>
    {
        /// <summary>
        /// Select keys from the state.
        /// </summary>
        public ISelectorWithoutProps<EntityState<TKey, TEntity>, List<TKey>> SelectIds { get; }

        /// <summary>
        /// Select collection (dictionary of entities) from the state.
        /// </summary>
        public ISelectorWithoutProps<EntityState<TKey, TEntity>, Dictionary<TKey, TEntity>> SelectCollection { get; }

        /// <summary>
        /// Select list of entities from the state.
        /// </summary>
        public ISelectorWithoutProps<EntityState<TKey, TEntity>, List<TEntity>> SelectEntities { get; }

        /// <summary>
        /// Select number of entities from the state.
        /// </summary>
        public ISelectorWithoutProps<EntityState<TKey, TEntity>, int> SelectCount { get; }

        ///// <summary>
        ///// Select an entity by id or null if it does not exist
        ///// </summary>
        public ISelectorWithProps<EntityState<TKey, TEntity>, TKey, TEntity> SelectEntityById { get; }

        internal EntitySelectors(
            IComparer<TEntity> sortComparer = null
        )
        {
            SelectIds = CreateSelector(
                (EntityState<TKey, TEntity> entityState) => entityState.Ids,
                new EqualityComparison.IdsComparer<TKey>()
            );

            SelectCollection = CreateSelector(
                (EntityState<TKey, TEntity> entityState) => entityState.Collection,
                new EqualityComparison.CollectionComparer<TKey, TEntity>()
            );

            SelectEntities = CreateSelector(
                SelectCollection,
                collection =>
                {
                    var result = collection
                        .Select(x => x.Value)
                        .ToList();

                    if (sortComparer != null)
                    {
                        result.Sort(sortComparer);
                    }

                    return result;
                },
                new EqualityComparison.EntitiesComparer<TEntity>()
            );

            SelectCount = CreateSelector(
                SelectCollection,
                collection => collection.Count
            );

            SelectEntityById = CreateSelector<EntityState<TKey, TEntity>, TKey, Dictionary<TKey, TEntity>, TEntity>(
                SelectCollection,
                (collection, id) => collection.TryGetValue(id, out TEntity item) ? item : default(TEntity)
            );
        }
    }

    /// <summary>
    /// Collection of selectors for an <see cref="EntityState{TKey, TEntity}"/>.
    /// </summary>
    /// <typeparam name="TInput">Part of the state used to create selectors.</typeparam>
    /// <typeparam name="TKey">Primary key of the entity.</typeparam>
    /// <typeparam name="TEntity">Type of the entity.</typeparam>
    public class EntitySelectors<TInput, TKey, TEntity>
    {
        /// <summary>
        /// Select keys from the state.
        /// </summary>
        public ISelectorWithoutProps<TInput, List<TKey>> SelectIds { get; }

        /// <summary>
        /// Select collection (dictionary of entities) from the state.
        /// </summary>
        public ISelectorWithoutProps<TInput, Dictionary<TKey, TEntity>> SelectCollection { get; }

        /// <summary>
        /// Select list of entities from the state.
        /// </summary>
        public ISelectorWithoutProps<TInput, List<TEntity>> SelectEntities { get; }

        /// <summary>
        /// Select number of entities from the state.
        /// </summary>
        public ISelectorWithoutProps<TInput, int> SelectCount { get; }

        ///// <summary>
        ///// Select an entity by id or null if it does not exist
        ///// </summary>
        public ISelectorWithProps<TInput, TKey, TEntity> SelectEntityById { get; }

        ///// <summary>
        ///// Select an set of entities by ids. Will return a List of the same size that includes null if the entities do not exist
        ///// </summary>
        public ISelectorWithProps<TInput, IEnumerable<TKey>, List<TEntity>> SelectEntitiesByIds { get; }

        ///// <summary>
        ///// Select an set of entities using the predicate to filter.
        ///// </summary>
        public ISelectorWithProps<TInput, Func<TEntity, bool>, List<TEntity>> SelectEntitiesWhere { get; }

        internal EntitySelectors(
            ISelectorWithoutProps<TInput, EntityState<TKey, TEntity>> selectEntityState,
            IComparer<TEntity> sortComparer = null
        )
        {
            SelectIds = CreateSelector(
                selectEntityState,
                entityState => entityState.Ids,
                new EqualityComparison.IdsComparer<TKey>()
            );

            SelectCollection = CreateSelector(
                selectEntityState,
                entityState => entityState.Collection,
                new EqualityComparison.CollectionComparer<TKey, TEntity>()
            );

            SelectEntities = CreateSelector(
                SelectCollection,
                collection =>
                {
                    var result = collection
                        .Select(x => x.Value)
                        .ToList();

                    if (sortComparer != null)
                    {
                        result.Sort(sortComparer);
                    }

                    return result;
                },
                new EqualityComparison.EntitiesComparer<TEntity>()
            );

            SelectCount = CreateSelector(
                SelectCollection,
                collection => collection.Count
            );

            SelectEntityById = CreateSelector<TInput, TKey, Dictionary<TKey, TEntity>, TEntity>(
                SelectCollection,
                (collection, id) => {
                    if (id == null) {
                        return default(TEntity);
                    }
                    return collection.TryGetValue(id, out TEntity item) ? item : default(TEntity);
                }
            );

            SelectEntitiesByIds = CreateSelector<TInput, IEnumerable<TKey>, Dictionary<TKey, TEntity>, List<TEntity>>(
                SelectCollection,
                (collection, ids) =>
                {
                    if (ids == null)
                    {
                        return new List<TEntity>();
                    }

                    var entities = ids.Select((id) =>
                    {
                        if (id == null)
                        {
                            return default(TEntity);
                        }
                        return collection.TryGetValue(id, out TEntity item) ? item : default(TEntity);
                    });

                    return entities.ToList();
                }
            );

            SelectEntitiesWhere = CreateSelector<TInput, Func<TEntity, bool>, Dictionary<TKey, TEntity>, List<TEntity>>(
                SelectCollection,
                (collection, predicate) =>
                {
                    return collection.Values.Where(predicate).ToList();
                }
            );
        }
    }

    namespace EqualityComparison
    {
        internal class CollectionComparer<TKey, TEntity> : EqualityComparer<Dictionary<TKey, TEntity>>
        {
            public override bool Equals(Dictionary<TKey, TEntity> x, Dictionary<TKey, TEntity> y)
            {
                var sameReference = ReferenceEquals(x, y);
                if (sameReference == true)
                {
                    return true;
                }

                if (x == null)
                {
                    return false;
                }

                if (y == null)
                {
                    return false;
                }

                if (x.Count != y.Count)
                {
                    return false;
                }

                // Check to see if anything in X is different than Y
                foreach (var pair in x)
                {
                    var key = pair.Key;
                    if (y.TryGetValue(key, out TEntity v) == true)
                    {
                        if (ReferenceEquals(pair.Value, v) == false)
                        {
                            return false;
                        }
                    }
                    else
                    {
                        return false;
                    }
                }

                // Check to see if anything in Y is different than X
                foreach (var pair in y)
                {
                    var key = pair.Key;
                    if (x.TryGetValue(key, out TEntity v) == true)
                    {
                        if (ReferenceEquals(pair.Value, v) == false)
                        {
                            return false;
                        }
                    }
                    else
                    {
                        return false;
                    }
                }

                return true;
            }

            public override int GetHashCode(Dictionary<TKey, TEntity> obj)
            {
                return obj.GetHashCode();
            }
        }

        internal class IdsComparer<TKey> : EqualityComparer<List<TKey>>
        {
            public override bool Equals(List<TKey> x, List<TKey> y)
            {
                var sameReference = ReferenceEquals(x, y);
                if (sameReference == true)
                {
                    return true;
                }

                if (x == null)
                {
                    return false;
                }

                if (y == null)
                {
                    return false;
                }

                if (x.Count != y.Count)
                {
                    return false;
                }

                var xNotY = x.Except(y).ToList();
                var yNotX = y.Except(x).ToList();

                return !xNotY.Any() && !yNotX.Any();
            }

            public override int GetHashCode(List<TKey> obj)
            {
                return obj.GetHashCode();
            }
        }

        internal class EntitiesComparer<TEntity> : EqualityComparer<List<TEntity>>
        {
            public override bool Equals(List<TEntity> x, List<TEntity> y)
            {
                var sameReference = ReferenceEquals(x, y);
                if (sameReference == true)
                {
                    return true;
                }

                if (x == null)
                {
                    return false;
                }

                if (y == null)
                {
                    return false;
                }

                if (x.Count != y.Count)
                {
                    return false;
                }

                var xNotY = x.Except(y).ToList();
                var yNotX = y.Except(x).ToList();

                return !xNotY.Any() && !yNotX.Any();
            }

            public override int GetHashCode(List<TEntity> obj)
            {
                return obj.GetHashCode();
            }
        }
    }
}
