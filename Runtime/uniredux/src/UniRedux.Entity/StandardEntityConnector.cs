﻿using System;
using System.Collections.Generic;
using System.Linq;
using UniRx;

namespace UniRedux.Entity
{
	public interface IWithChanges<TEntity> where TEntity : class
	{
		IObservable<TEntity> Changed { get; }
	}

	public static class WithChanges
	{
		public static IDisposable SetAndOnChanged<TEntity>(TEntity entity, Action<TEntity> onEvent) where TEntity : class, IWithChanges<TEntity>
		{
			onEvent(entity);
			var sub = entity.Changed.Subscribe((updated) =>
			{
				onEvent(updated);
			});
			return sub;
		}

		public static IObservable<TEntity?> IncludeChanges<TEntity>(this IObservable<TEntity?> source) where TEntity : class, IWithChanges<TEntity>
		{
			return source.SelectMany(e => e?.Changed ?? Observable.Return<TEntity?>(null));
		}

		public static IObservable<IList<TEntity>> IncludeChanges<TEntity>(this IObservable<IList<TEntity?>> source) where TEntity : class, IWithChanges<TEntity>
		{
			return source.SelectMany(list => list.Where(e => e != null).Select(e => e.Changed).Merge().Select(_ => list));
		}
	}

	public class StandardEntityConnector<TRootState, TEntity> where TRootState : class, new() where TEntity : class
	{
		public class EntitiesState : EntityState<string, TEntity> { }

		private Func<TRootState, EntitiesState> GetEntitiesStateFromContainer;
		private Func<TRootState, EntitiesState, TRootState> SetEntitiesStateToContainer;
		public readonly EntityAdapter<string, TEntity> EntitiesAdapter;

		public readonly ISelectorWithoutProps<TRootState, EntitiesState> SelectEntitiesState;

		private readonly EntitySelectors<TRootState, string, TEntity> EntitiesSelectors;

		public readonly ISelectorWithoutProps<TRootState, List<string>> SelectIds;
		public readonly ISelectorWithoutProps<TRootState, Dictionary<string, TEntity>> SelectCollection;
		public readonly ISelectorWithoutProps<TRootState, List<TEntity>> SelectEntities;
		public readonly ISelectorWithoutProps<TRootState, int> SelectCount;
		public readonly ISelectorWithProps<TRootState, string?, TEntity?> SelectEntityById;
		public readonly ISelectorWithProps<TRootState, IEnumerable<string?>?, List<TEntity?>> SelectEntitiesByIds;
		public readonly ISelectorWithProps<TRootState, Func<TEntity, bool>, List<TEntity>> SelectEntitiesWhere;

		public StandardEntityConnector(Func<TRootState, EntitiesState> getEntitiesStateFromContainer, Func<TRootState, EntitiesState, TRootState> setEntitiesStateToContainer, Func<TEntity, string> idFromInstance)
		{
			this.GetEntitiesStateFromContainer = getEntitiesStateFromContainer;
			this.SetEntitiesStateToContainer = setEntitiesStateToContainer;
			this.EntitiesAdapter = EntityAdapter<string, TEntity>.Create(idFromInstance);
			this.SelectEntitiesState = Selectors.CreateSelector(this.GetEntitiesStateFromContainer);

			this.EntitiesSelectors = this.EntitiesAdapter.GetSelectors(this.SelectEntitiesState);
			this.SelectIds = this.EntitiesSelectors.SelectIds;
			this.SelectCollection = this.EntitiesSelectors.SelectCollection;
			this.SelectEntities = this.EntitiesSelectors.SelectEntities;
			this.SelectCount = this.EntitiesSelectors.SelectCount;
			this.SelectEntityById = this.EntitiesSelectors.SelectEntityById;
			this.SelectEntitiesByIds = this.EntitiesSelectors.SelectEntitiesByIds;
			this.SelectEntitiesWhere = this.EntitiesSelectors.SelectEntitiesWhere;
		}

		public IObservable<List<TEntity>> GetEntitiesFilteredBy<TFilter>(ReduxStore<TRootState> store, IObservable<TFilter?> filter, Func<TEntity, TFilter?, bool> predicate) where TFilter : class
		{
			return filter.SelectMany((f) =>
			{
				return store.Select(this.SelectEntitiesWhere, (entity) => predicate(entity, f));
			});
		}

		public IDisposable RegisterForAddAll(IReduxStore store, IObservable<IEnumerable<TEntity>> obs)
		{
			var sub = obs.Subscribe(entities =>
			{
				store.Dispatch(new AddAll(entities));
			});

			return sub;
		}

		public class AddAll : ReduxAction<AddAll>
		{
			public IEnumerable<TEntity>? Entities;

			public AddAll(IEnumerable<TEntity> entities)
			{
				this.Entities = entities;
			}

			public AddAll() { }
		}

		public class UpsertOne : ReduxAction<UpsertOne>
		{
			public TEntity? Entity;

			public UpsertOne(TEntity entity)
			{
				this.Entity = entity;
			}

			public UpsertOne() { }
		}

		public class UpsertMany : ReduxAction<UpsertMany>
		{
			public IEnumerable<TEntity>? Entities;

			public UpsertMany(IEnumerable<TEntity> entities)
			{
				this.Entities = entities;
			}

			public UpsertMany() { }
		}

		public class RemoveOne : ReduxAction<RemoveOne>
		{
			public TEntity? Entity;
			public string? Id;

			public RemoveOne(TEntity entity)
			{
				this.Entity = entity;
			}

			public RemoveOne(string id)
			{
				this.Id = id;
			}

			public RemoveOne() { }
		}

		public class RemoveMany : ReduxAction<RemoveMany>
		{
			public IEnumerable<TEntity>? Entities;
			public IEnumerable<string>? Ids;

			public RemoveMany(IEnumerable<TEntity> entities)
			{
				this.Entities = entities;
			}

			public RemoveMany(IEnumerable<string> ids)
			{
				this.Ids = ids;
			}

			public RemoveMany() { }
		}

		public class RemoveAll : ReduxAction<RemoveAll>
		{
			public RemoveAll() { }
		}

		public IReadOnlyList<On<TRootState>> CreateReducers()
		{
			return new List<On<TRootState>>
			{
				Reducers.On<AddAll, TRootState>(
					(state, action) => {
						if( action.Entities == null)
						{
							return state;
						}


						var entities = this.EntitiesAdapter.AddAll(action.Entities, this.GetEntitiesStateFromContainer(state));
						return this.SetEntitiesStateToContainer(state, entities);
					}
				),

				Reducers.On<UpsertOne, TRootState>(
					(state, action) => {
						if( action.Entity == null)
						{
							return state;
						}

						var entities = this.EntitiesAdapter.UpsertOne(action.Entity, this.GetEntitiesStateFromContainer(state));
						return this.SetEntitiesStateToContainer(state, entities);
					}
				),

				Reducers.On<UpsertMany, TRootState>(
					(state, action) => {
						if( action.Entities == null)
						{
							return state;
						}

						var entities = this.EntitiesAdapter.UpsertMany(action.Entities, this.GetEntitiesStateFromContainer(state));
						return this.SetEntitiesStateToContainer(state, entities);
					}
				),

				Reducers.On<RemoveOne, TRootState>(
					(state, action) => {
						if( ( action.Entity == null ) && ( string.IsNullOrEmpty( action.Id ) ) )
						{
							return state;
						}

						var id = ( action.Entity != null ) ? this.EntitiesAdapter.SelectId(action.Entity) : action.Id;
						var entities = this.EntitiesAdapter.RemoveOne(id, this.GetEntitiesStateFromContainer(state));
						return this.SetEntitiesStateToContainer(state, entities);
					}
				),

				Reducers.On<RemoveMany, TRootState>(
					(state, action) => {
						if( ( action.Entities == null ) && ( action.Ids == null ) )
						{
							return state;
						}

						var ids = ( action.Ids != null ) ? new List<string>(action.Ids) : new List<string>();
						if( action.Entities != null )
						{
							ids.AddRange( action.Entities.Select(this.EntitiesAdapter.SelectId) );
						}

						var entities = this.EntitiesAdapter.RemoveMany(ids, this.GetEntitiesStateFromContainer(state));
						return this.SetEntitiesStateToContainer(state, entities);
					}
				),

				Reducers.On<RemoveAll, TRootState>(
					(state, action) => {
						var entities = this.EntitiesAdapter.RemoveAll(this.GetEntitiesStateFromContainer(state));
						return this.SetEntitiesStateToContainer(state, entities);
					}
				)
			};
		}
	}
}
