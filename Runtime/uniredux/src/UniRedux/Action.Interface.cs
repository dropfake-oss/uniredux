namespace UniRedux
{
	/// <summary>
	/// A base interface for every type of action.
	/// </summary>
	public interface IReduxAction { }
}
