﻿namespace UniRedux
{
    /// <summary>
    /// Action dispatched when the store is initialized.
    /// </summary>
    public class InitializeStoreAction { }

    /// <summary>
    /// A base class to use for ReduxActions to ensure they have an empty constructor
    /// </summary>
    /// <typeparam name="Klass"></typeparam>
	public class ReduxAction<Klass> : IReduxAction where Klass : class, new() { }
}
