﻿using System;
using System.Linq;
using UniRx;

namespace UniRedux
{
    public class StoreSelectOptions
    {
        public bool OnCurrentThread;
    }

    public interface ISelectableReduxStore<TState> : IReduxStore where TState : class, new()
    {
        public IObservable<TState> Select();
        public IObservable<TResult> Select<TResult>(Func<TState, TResult> selector);
        public IObservable<TResult> Select<TResult>(ISelectorWithoutProps<TState, TResult> selector, StoreSelectOptions? options = null);
        public IObservable<TResult> Select<TProps, TResult>(ISelectorWithProps<TState, TProps, TResult> selector, TProps props, StoreSelectOptions? options = null);
    }

    /// <summary>
    /// The <see cref="ReduxStore{TState}" /> is a centralized object for creating predictable state containers.
    /// </summary>
    /// <typeparam name="TState">The type of the state.</typeparam>
    public sealed partial class ReduxStore<TState> : ISelectableReduxStore<TState> where TState : class, new()
    {
        public class SelectOptions
        {
            public bool OnCurrentThread;
        }

        private readonly FullStateComparer<TState> _fullStateComparer = new FullStateComparer<TState>();

        /// <summary>
        /// Select the full state object.
        /// </summary>
        /// <returns>An <see cref="IObservable{T}"/> that can be subscribed to in order to receive updates about state changes.</returns>
        public IObservable<TState> Select()
        {
            return _stateSubject
                .DistinctUntilChanged(_fullStateComparer);
        }
        /// <summary>
        /// Select a value derived from the state of the store.
        /// </summary>
        /// <typeparam name="TResult">The type of the selector result.</typeparam>
        /// <param name="selector">
        /// The mapping function that can be applied to get the desired partial state of type <typeparamref name="TResult"/> from an instance of <typeparamref name="TState"/>.
        /// </param>
        /// <returns></returns>
        public IObservable<TResult> Select<TResult>(Func<TState, TResult> selector)
        {
            return _stateSubject
                .Select(selector)
                .DistinctUntilChanged();
        }
        /// <summary>
        /// Select a value derived from the state of the store.
        /// </summary>
        /// <typeparam name="TResult">The type of the selector result.</typeparam>
        /// <param name="selector">
        /// The mapping function that can be applied to get the desired partial state of type <typeparamref name="TResult"/> from an instance of <typeparamref name="TState"/>.
        /// </param>
        /// <returns></returns>
        public IObservable<TResult> Select<TResult>(ISelectorWithoutProps<TState, TResult> selector, StoreSelectOptions? options = null)
        {
            return AdjustForOptions(selector.Apply(_stateSubject), options);
        }

        /// <summary>
        /// Select a value derived from the state of the store.
        /// </summary>
        /// <typeparam name="TProps">The type of the props to use in the selector.</typeparam>
        /// <typeparam name="TResult">The type of the selector result.</typeparam>
        /// <param name="selector">
        /// The mapping function that can be applied to get the desired partial state of type <typeparamref name="TResult"/> from an instance of <typeparamref name="TState"/>.
        /// </param>
        /// <param name="props">
        /// The properties used in the selector.
        /// </param>
        /// <returns></returns>
        public IObservable<TResult> Select<TProps, TResult>(ISelectorWithProps<TState, TProps, TResult> selector, TProps props, StoreSelectOptions? options = null)
        {
            return AdjustForOptions(selector.Apply(_stateSubject, props), options);
        }

        private IObservable<TResult> AdjustForOptions<TResult>(IObservable<TResult> obs, StoreSelectOptions? options)
        {
            var onMainThread = true;
            if (options != null)
            {
                onMainThread = !options.OnCurrentThread;
            }

            if (onMainThread == false)
            {
                return obs;
            }

#if UNITY_2020_1_OR_NEWER
			obs = obs.ObserveOnMainThread();
#endif
            return obs;
        }
    }
}
