﻿using System;
using System.Collections.Generic;

namespace UniRedux
{
    public static partial class Selectors
    {
        /// <summary>
        /// Create a new simple selector.
        /// </summary>
        /// <typeparam name="TState">State to consume.</typeparam>
        /// <typeparam name="TResult">Result of the selector.</typeparam>
        /// <param name="selector">Selector function.</param>
        /// <param name="comparer">Comparer that is used to determine if the TResult has changed</param>
        /// <returns>A new selector.</returns>
        public static ISelectorWithoutProps<TState, TResult> CreateSelector<TState, TResult>(
            Func<TState, TResult> selector,
            IEqualityComparer<TResult> comparer = null
        )
        {
            return new SimpleSelector<TState, TResult>(
                selector,
                comparer
            );
        }
    }
}
