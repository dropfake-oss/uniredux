// Copyright 2021-present Drop Fake Inc. All rights reserved.

using System;
using System.Collections.Generic;
using System.Linq;
using UniRx;

namespace UniRedux.Utils
{
    public class ManagerWithStore<Klass, TState> where Klass : ManagerWithStore<Klass, TState>, new() where TState : class, new()
    {
        [Serializable]
        public class NotInitialized : Exception { }

        private static BehaviorSubject<Klass?> InstanceSubject = new BehaviorSubject<Klass?>(null);
        public static Klass Instance
        {
            get
            {
                var instance = InstanceSubject.Value;
                if (instance == null)
                {
                    throw new NotInitialized();
                }
                return instance;
            }
        }
        public static IObservable<Klass> AsObservable => InstanceSubject.Where(instance => instance != null);

        public ReduxStore<TState>? Store { get; private set; } = null;
        public static IObservable<ReduxStore<TState>> StoreAsObservable => AsObservable.Select((instance) => instance.Store);

        public static bool IsInitialized => InstanceSubject.Value != null;
        public static Klass Initialize(IEnumerable<On<TState>> reducers, TState initialState, IEnumerable<Effect<TState>>? effects = null, bool enableTimeTravel = false)
        {
            var instance = new Klass();
            instance.Store = new ReduxStore<TState>(reducers, initialState, enableTimeTravel);

            if (effects != null)
            {
                var asArray = effects.ToArray();
                if (asArray.Length > 0)
                {
                    instance.Store.RegisterEffects(asArray);
                }
            }

            InstanceSubject.OnNext(instance);
            return instance;
        }

        public static IObservable<TResult> Select<TResult>(ISelectorWithoutProps<TState, TResult> selector, StoreSelectOptions? options = null)
        {
            return StoreAsObservable.Select((store) => store.Select(selector, options)).Switch();
        }

        protected ManagerWithStore()
        {
        }
    }
}
